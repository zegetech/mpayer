require 'wsse'
require 'net/https'
class Transaction
  #@@root_url = "https://ec2-72-44-42-20.compute-1.amazonaws.com/api/transactions"
  @uri = URI.parse("https://ec2-72-44-42-20.compute-1.amazonaws.com/api/transactions")
  @@http = Net::HTTP.new(@uri.host, @uri.port)
  @@http.use_ssl = true
  @@http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  def initialize(user_no, token)
    @user_no = user_no
    @token = token
    @headers = {'Content-Type'=>'application/xml','X-WSSE' => WSSE::header(@user_no, @token)}
  end

  def all_trans
    @all_trans_req = Net::HTTP::Get.new("/all.json", @headers)
    @all_trans_res = @@http.request(@all_trans_req)
    @all_trans_res_body = @all_trans_res.body
    p  @all_trans_res_body
    p @all_trans_res
  end

  def single_trans
    @single_trans_link = "/:#{@ref_id}"
    @single_trans_url = URI.parse("#{@@root_url}#{@single_trans_link}")
    @single_trans_http = Net::HTTP.new(@single_trans_url.host,@single_trans_url.port)
    @single_trans_http.use_ssl = true
    @single_trans_http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    @single_trans_res = @single_trans_http.get(@single_trans_url.path,@headers)
    @single_trans_res_body = @single_trans_res.body
    p  @single_trans_res_body
    p @single_trans_res
  end

#  def dep
#    @dep_link = "/deposit.json"
#    @dep_url = URI.parse("#{@@root_url}#{@dep_link}")
#    @dep_http = Net::HTTP.new(@dep_url.host,@dep_url.port)
#    @dep_http.use_ssl = true
#    @dep_http.verify_mode = OpenSSL::SSL::VERIFY_NONE
#    @dep_res = @dep_http.put(@dep_url.path,@headers)
#    @dep_res_body = @dep_res.body
#    p  @dep_res_body
#    p @dep_res
#  end

  def with
#    @with_link = "/withdraw.json"
#    @with_url = URI.parse("#{@@root_url}#{@with_link}")
#    @with_http = Net::HTTP.new(@with_url.host,@with_url.port)
#    @with_http.use_ssl = true
#    @with_http.verify_mode = OpenSSL::SSL::VERIFY_NONE
#    @with_res = @with_http.delete(@with_url.path,@headers)
    @with_res_body = @with_res.body
    @with_request = Net::HTTP::Delete.new("/withdraw.json", @headers)
    @with_res = @http.request(@with_request)
#request = Net::HTTP::Delete.new("/users/1")
#response = http.request(request)
    p  @with_res_body
    p @with_res
  end
end
t = Transaction.new("NC0014", "7DHeBCt9Ka6337owqDSn")
t.all_trans
#t.dep
